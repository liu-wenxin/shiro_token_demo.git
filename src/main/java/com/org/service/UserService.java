package com.org.service;

import com.org.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 刘文鑫
 * @since 2020-12-12
 */
public interface UserService extends IService<User> {
    //根据账号查找用户
    User findByUsername(String username);
}
