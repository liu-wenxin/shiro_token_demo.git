package com.org.mapper;

import com.org.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 刘文鑫
 * @since 2020-12-12
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

}
