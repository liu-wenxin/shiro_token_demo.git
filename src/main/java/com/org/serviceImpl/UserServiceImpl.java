package com.org.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.org.entity.User;
import com.org.mapper.UserMapper;
import com.org.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 刘文鑫
 * @since 2020-12-12
 */

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    UserMapper userMapper;

    @Override
    public User findByUsername(String username) {
        //相当于select * from user where account='${username}'
        QueryWrapper<User> wrapper=new QueryWrapper<>();
        wrapper.eq("account",username);
        //user即为查询结果
        return userMapper.selectOne(wrapper);
    }
}
